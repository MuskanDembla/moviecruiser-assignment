
var movies=[];
// var favourites=[];

function getMovies(){

	var xhr=new XMLHttpRequest();
	xhr.open("get","http://localhost:3000/movies");
	xhr.send(null)

	xhr.onload= function() {
		movies=JSON.parse(xhr.responseText);
		console.log(movies);
		for(let i=0;i<movies.length;i++){
			document.getElementById("moviesList").innerHTML+=
			`<div class="product">
			<div><h5>${movies[i].title}</h5></div>
			<img src=${movies[i].source} alt="${movies[i].title}"></img><br><br>
			<button type="button" class="btn btn-primary" onclick="addFavourites(${movies[i].id})">Add to Fav </button><br>
			</div><br>`
		}
	}
}

function getFavourites(){
	var movies=[];

	var xhr = new XMLHttpRequest();
	console.log(xhr.readyState);
	xhr.open("get","http://localhost:3000/favourites");
	xhr.send(null)
	xhr.onload= function() {
		favourites=JSON.parse(xhr.responseText);
		console.log(favourites);
		for(let i=0;i<favourites.length;i++){
			document.getElementById("favouritesList").innerHTML+=
			`<div class="fav">
			<div><h5>${favourites[i].title}</h5></div>
			<img src=${favourites[i].source}></img><br><br>
			<button type="button" class="btn btn-primary" onclick="deleteFavourites(${favourites[i].id})">Remove from Fav</button>
			</div> <br>`
		}
	}
}

function addFavourites(id){
	console.log("Added to favourite");
	let obj = getMovieInfoById(id);

	return fetch(" http://localhost:3000/favourites",
	{
		method:'POST',
		body:JSON.stringify(obj),
		headers :{
			"Content-Type": "application/json"
		}
	})
	.then(res=>res.json())
	.then(json => console.log(id))

}
const getMovieInfoById = (movieId)=>{
	for(let i=0;i<movies.length;i++){
		if(movieId==movies[i].id){
			console.log(movies[i]);
			return movies[i];
		}
	}
}

const deleteFavourites =(favId) =>{
	console.log(`Movie id =${favId} deleted`);
	return fetch(` http://localhost:3000/favourites/${favId}`,
	{
		method:'DELETE',
		headers:{"Content-Type":"application/json"}
	})
}

// module.exports={
// 	getMovies,
// 	getFavourites,
// 	addFavourites,
// 	deleteFavourites
//  };